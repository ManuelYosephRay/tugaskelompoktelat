# from .views import profileUtama, musings, schedules, schedulesview, schedulesedit, schedulesdelete
from . import views
from django.urls import path

app_name= 'contact'

urlpatterns = [
	# path('', views.profileUtama, name='home'),
	path('contact/', views.contact, name='contact' ),
	# path('schedules-add/', views.schedules_add, name='schedules_add'),
	# path('schedulesView/', views.schedulesview, name ="schedulesview"),
 #    path('schedules/delete/<int:id>', views.schedulesdelete, name='schedulesdelete'),
]