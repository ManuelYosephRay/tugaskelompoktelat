from django.contrib import admin
from .models import Contact
# Register your models here.

class ContactAdmin(admin.ModelAdmin):
	list_display = ['kategori', 'judul', 'isi', 'email','updated']
	list_filter = ['updated']

admin.site.register(Contact, ContactAdmin)
