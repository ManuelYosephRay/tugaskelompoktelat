# Generated by Django 2.0.6 on 2019-10-18 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kategori', models.CharField(choices=[('Keluhan', 'Keluhan'), ('Pembayaran', 'Pembayaran'), ('Lainnya', 'Lainnya')], default='Keluhan', max_length=50)),
                ('judul', models.CharField(max_length=250)),
                ('isi', models.TextField()),
                ('email', models.CharField(max_length=250)),
                ('updated', models.DateTimeField()),
            ],
        ),
    ]
