from django.shortcuts import render, redirect


from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import ContactForm
from .models import Contact
import pprint
import datetime
import time

# def contact(request):
# 	return render(request, 'contact.html')	

def contact(request):
    # pprint.pprint(request.POST)
    if request.method == 'POST':
       form = ContactForm(request.POST)
       if form.is_valid():
          contact = form.save(commit=False)
          contact.updated = datetime.datetime.now()
          contact.save()
          return HttpResponseRedirect('')
    else:
       form = ContactForm()
    return render(request, 'contact.html', {'form': form})
