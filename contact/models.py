from django.conf import settings
from django.db import models
from django.urls import reverse


class Contact(models.Model):
      cat 		=  (
			        ('Keluhan', 'Keluhan'),
			        ('Pembayaran', 'Pembayaran'),
			        ('Lainnya', 'Lainnya'),
				    )
      kategori = models.CharField(max_length=50, choices=cat, default='Keluhan')
      judul = models.CharField(max_length=250)
      isi = models.TextField()
      email = models.CharField(max_length=250)
      updated = models.DateTimeField()  
      def __str__(self):
          return self.judul





