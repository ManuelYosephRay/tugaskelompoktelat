from django.shortcuts import render
from .forms import ReviewForm
from django.contrib import messages

# Create your views here.
def homeReview(request):
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            form = ReviewForm()
            messages.success(request, 'Review sebelumnya sudah berhasil terkirim!') 
    else:
        form = ReviewForm()
    return render(request,'homeReview.html',{'form':form})
