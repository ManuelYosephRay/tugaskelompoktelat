from django.conf import settings
from django.db import models
from django.utils import timezone


class Review(models.Model):
    tanggapan = models.TextField(u'Bagaimana tanggapanmu?')
    saran = models.TextField(u'Apakah ada fitur yang perlu diubah?')

    # def publish(self):
    #     self.published_date = timezone.now()
    #     self.save()

    # def __str__(self):
    #     return self.title