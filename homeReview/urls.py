from django.urls import path
from . import views

app_name = 'homeReview'

urlpatterns = [
    path('',views.homeReview,name='homeReview'),
]