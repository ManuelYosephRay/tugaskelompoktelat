from django.db import models


class jadwalAppointment(models.Model):
	DOKTER= (
		("1","Meldi"),
		('2',"Safa"),
		('3',"Nuel"),
		('4',"Bella"),
		)
	nama = models.CharField(max_length= 50)
	tanggal = models.DateField()
	waktu = models.TimeField(auto_now=True)
	keluhan =models.TextField()
	dokter = models.CharField(max_length = 100,choices=DOKTER)
	status = models.CharField(max_length=50, default="Pending")

	def __str__(self):
		return "{}. {}".format(self.id,self.nama)
		
# Create your models here.
