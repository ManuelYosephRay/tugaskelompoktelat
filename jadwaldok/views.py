from django.shortcuts import render, redirect
from . import models, forms
from .forms import formAppointment
from .models import jadwalAppointment

# Create your views here.
def jadwal(request):
	createAppointment = formAppointment(request.POST or None)
	dataAppoint= jadwalAppointment.objects.all()
	context = {
		"appointments":dataAppoint,
		"create_jadwal":createAppointment,
	}
	if request.method=="POST":
		if createAppointment.is_valid():
			jadwalAppointment.objects.create(
				nama = createAppointment.cleaned_data.get('nama'),
				tanggal = createAppointment.cleaned_data.get('tanggal'),
				waktu =createAppointment.cleaned_data.get('waktu'),
				keluhan = createAppointment.cleaned_data.get('keluhan'),
				dokter = createAppointment.cleaned_data.get('dokter'),
				)
	return render(request,"pages/jadwal.html",context)

	# if request.method = "POST":
	# 	if createAppointment.is_valid():
	# 		