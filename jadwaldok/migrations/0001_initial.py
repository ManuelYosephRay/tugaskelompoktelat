# Generated by Django 2.0.6 on 2019-10-17 09:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='jadwalAppointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
                ('tanggal', models.DateField()),
                ('waktu', models.TimeField(auto_now=True)),
                ('keluhan', models.TextField()),
                ('dokter', models.CharField(choices=[('1', 'Meldi'), ('2', 'Safa'), ('3', 'Nuel'), ('4', 'Bella')], max_length=100)),
            ],
        ),
    ]
