from django.apps import AppConfig


class JadwaldokConfig(AppConfig):
    name = 'jadwaldok'
