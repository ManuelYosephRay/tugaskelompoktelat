from django import forms
from . import models

class formAppointment(forms.Form):
	nama = forms.CharField(required=True)
	tanggal = forms.DateField(
		widget=forms.SelectDateWidget)
	waktu = forms.TimeField()
	keluhan = forms.CharField()
	dokter = forms.ChoiceField(choices=models.jadwalAppointment.DOKTER)
		