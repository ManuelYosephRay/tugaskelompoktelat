from django.urls import path
from . import views

app_name='jadwaldok'

urlpatterns = [
    path('jadwal/',views.jadwal,name='jadwal'),
]