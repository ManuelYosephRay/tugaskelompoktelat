from django.urls import path
from . import views

app_name='carisolusi'

urlpatterns = [
    path('carisolusi/',views.carisolusi,name='carisolusi'),
    path('hasilsolusi/',views.hasilsolusi,name='hasilsolusi'),
    path('hasilsolusi2/',views.hasilsolusi2,name='hasilsolusi2'),
    path('hasilsolusi3/',views.hasilsolusi3,name='hasilsolusi3'),
]