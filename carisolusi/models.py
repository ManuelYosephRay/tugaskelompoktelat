from django.db import models


penyakit1 = (
    (1,"Sakit kepala"),
    (2,"Muntah-Muntah"),
    (3,"Kulit pucat")
)
penyakit2 = (
    (4,"Suhu badan tinggi"),
    (5,"Warna urine berubah"),
    (6,"Sesak napas")
)
penyakit3 = (
    (7,"Sesak napas"),
    (8,"Nyeri punggung"),
    (9,"Batuk-batuk")
)
# Create your models here.
class Penyakit(models.Model):

    Keluhan1 = models.IntegerField(choices = penyakit1)
    Keluhan2 = models.IntegerField(choices = penyakit2)
    Keluhan3 = models.IntegerField(choices = penyakit3)
    def __str__(self):
        return self.Keluhan1