from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import HasilPenyakit
from . import forms

# Create your views here.
def carisolusi(request):
    if request.method == 'POST':
        form = HasilPenyakit(request.POST)
        if form.is_valid():
            kel1 = request.POST.get('Keluhan1')
            kel2 = request.POST.get('Keluhan2')
            kel3 = request.POST.get('Keluhan3')
            if int(kel1) == 1:
                return redirect('/hasilsolusi/')
            elif int(kel1) == 2:
                return redirect('/hasilsolusi2/')
            else:
                return redirect('/hasilsolusi3/')
    else:
        form = HasilPenyakit()
    return render(request, 'carisolusi.html', {'form': form})

def hasilsolusi(request):
    return render(request, 'hasilsolusi.html')
def hasilsolusi2(request):
    return render(request, 'hasilsolusi2.html')
def hasilsolusi3(request):
    return render(request, 'hasilsolusi3.html')

    #blabla