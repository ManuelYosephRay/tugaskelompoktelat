from django import forms
from .models import Penyakit

class HasilPenyakit(forms.ModelForm):
    class Meta:
        model = Penyakit
        fields = ['Keluhan1', 'Keluhan2', 'Keluhan3']
